-- phpMyAdmin SQL Dump
-- version 4.1.4
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 17, 2019 alle 09:24
-- Versione del server: 5.6.15-log
-- PHP Version: 5.5.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `bikesharing`
--
CREATE DATABASE IF NOT EXISTS `bikesharing` DEFAULT CHARACTER SET latin1 COLLATE latin1_general_cs;
USE `bikesharing`;

-- --------------------------------------------------------

--
-- Struttura della tabella `biciclette`
--

CREATE TABLE IF NOT EXISTS `biciclette` (
  `id_bicicletta` int(30) NOT NULL AUTO_INCREMENT,
  `id_stazione` int(30) DEFAULT NULL,
  PRIMARY KEY (`id_bicicletta`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_cs AUTO_INCREMENT=16 ;

--
-- Dump dei dati per la tabella `biciclette`
--

INSERT INTO `biciclette` (`id_bicicletta`, `id_stazione`) VALUES
(10, 7),
(9, 7),
(8, 8),
(7, 8),
(6, 7),
(11, 7),
(12, 8),
(13, 8),
(14, 7),
(15, 7);

-- --------------------------------------------------------

--
-- Struttura della tabella `metodipagamento`
--

CREATE TABLE IF NOT EXISTS `metodipagamento` (
  `id_metodo` int(30) NOT NULL AUTO_INCREMENT,
  `id_utente` int(30) NOT NULL,
  `n_carta` varchar(30) COLLATE latin1_general_cs NOT NULL,
  `cvv` varchar(3) COLLATE latin1_general_cs NOT NULL,
  `expiry` varchar(5) COLLATE latin1_general_cs NOT NULL,
  PRIMARY KEY (`id_metodo`),
  UNIQUE KEY `n_carta` (`n_carta`),
  KEY `id_utente` (`id_utente`),
  KEY `n_carta_2` (`n_carta`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_cs AUTO_INCREMENT=2 ;

--
-- Dump dei dati per la tabella `metodipagamento`
--

INSERT INTO `metodipagamento` (`id_metodo`, `id_utente`, `n_carta`, `cvv`, `expiry`) VALUES
(1, 2, '?', '?', '?');

-- --------------------------------------------------------

--
-- Struttura della tabella `noleggi`
--

CREATE TABLE IF NOT EXISTS `noleggi` (
  `id_noleggio` int(30) NOT NULL AUTO_INCREMENT,
  `id_utente` int(30) NOT NULL,
  `id_bicicletta` int(30) NOT NULL,
  `data_noleggio` date NOT NULL,
  `id_stazione_noleggio` int(30) NOT NULL,
  `data_restituzione` date DEFAULT NULL,
  `id_stazione_restituzione` int(30) DEFAULT NULL,
  PRIMARY KEY (`id_noleggio`),
  KEY `id_utente` (`id_utente`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_cs AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `stazioni`
--

CREATE TABLE IF NOT EXISTS `stazioni` (
  `id_stazione` int(30) NOT NULL AUTO_INCREMENT,
  `nome` varchar(30) COLLATE latin1_general_cs NOT NULL,
  `nbici` int(11) NOT NULL,
  `lat` float(10,5) NOT NULL,
  `lon` float(10,5) NOT NULL,
  PRIMARY KEY (`id_stazione`),
  KEY `nome` (`nome`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_cs AUTO_INCREMENT=9 ;

--
-- Dump dei dati per la tabella `stazioni`
--

INSERT INTO `stazioni` (`id_stazione`, `nome`, `nbici`, `lat`, `lon`) VALUES
(8, 'Via Anna 8', 0, 45.55959, 8.04750),
(7, 'Via Profranco 1', 0, 45.56661, 8.04604);

-- --------------------------------------------------------

--
-- Struttura della tabella `utenti`
--

CREATE TABLE IF NOT EXISTS `utenti` (
  `id_utente` int(30) NOT NULL AUTO_INCREMENT,
  `nome` varchar(30) COLLATE latin1_general_cs NOT NULL,
  `cognome` varchar(30) COLLATE latin1_general_cs NOT NULL,
  `indirizzo` varchar(50) COLLATE latin1_general_cs NOT NULL,
  `citta` varchar(30) COLLATE latin1_general_cs NOT NULL,
  `id_tessera` int(30) DEFAULT NULL,
  `username` varchar(30) COLLATE latin1_general_cs NOT NULL,
  `password` varchar(30) COLLATE latin1_general_cs NOT NULL,
  `email` varchar(30) COLLATE latin1_general_cs NOT NULL,
  PRIMARY KEY (`id_utente`),
  KEY `username` (`username`,`email`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_cs AUTO_INCREMENT=4 ;

--
-- Dump dei dati per la tabella `utenti`
--

INSERT INTO `utenti` (`id_utente`, `nome`, `cognome`, `indirizzo`, `citta`, `id_tessera`, `username`, `password`, `email`) VALUES
(2, 'Alessio', 'Esposito Minchiostro', 'Via le dita dal naso 1', 'Sandigliano', NULL, 'Espo', 'stcs4yTT3qohY', 'ale@espo.it'),
(3, '', '', '', '', NULL, '', 'stNPuLMaoIxdU', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
