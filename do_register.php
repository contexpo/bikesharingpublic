<?php
include_once "functions.php";
$conn = getDBConnection();
// verifica su eventuali errori di connessione
if ($conn->connect_errno) {
    echo "Connessione fallita: ". $conn->connect_error . ".";
    exit();
}
$password=crypt($_POST["password"], "stringadisalt");
$query="INSERT INTO utenti(nome, cognome, indirizzo, citta, username, `password`, email) VALUES (?, ?, ?, ?, ?, ?, ?)";
$sql = $conn->stmt_init();
$sql->prepare ($query);
$sql->bind_param("sssssss", $_POST["nome"], $_POST["cognome"], $_POST["address"], $_POST["city"], $_POST["username"], $password, $_POST["email"]);
$sql->execute();
$result=$sql->get_result();
header('Location:/bikesharing/login.php');
?>  