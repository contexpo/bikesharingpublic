<?php
session_start();
if (!isset($_SESSION["id_user"]))
{
    header("location: /bikesharing/login.php");
}

include_once "functions.php";
$connessione=getDBConnection();
if ($connessione->connect_errno)
{
    echo "Connessione fallita: ". $connessione->connect_error . ".";
    exit();
}
$query="SELECT id_noleggio FROM noleggi WHERE(id_utente=? AND pagato=0)";
$sql = $connessione->stmt_init();
$sql->prepare ($query);
$sql->bind_param("i", $_SESSION['id_user']);
$sql->execute();
$result=$sql->get_result();
if ($result===FALSE)
{
    exit ("query fallita");
}
$row=$result->fetch_assoc();
$query="UPDATE noleggi SET pagato=1 WHERE id_noleggio=? and  id_bicicletta=?";
$sql = $connessione->stmt_init();
$sql->prepare ($query);
$sql->bind_param("iii", $_POST['Colonnina'], $row["id_noleggio"], $_POST["bici"]);
$sql->execute();
header("location: /bikesharing/restituzione.php");
?>
