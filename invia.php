<?php
session_start();
include_once 'fpdf/fpdf.php'; "libraries.php"; 
if (!isset($_SESSION["id_user"]))
{
  header("location: /bikesharing/login.php");
}
include_once "functions.php";
$conn = getDBConnection();
if ($conn->connect_errno)
{
  echo "connessione fallita: " . $conn->connect_error . ".";
  exit();
}
$query="SELECT * FROM`noleggi`WHERE `id_utente`=?";
$sql = $conn->stmt_init();
$sql->prepare ($query);
$sql->bind_param("i", $_SESSION['id_user']);
$sql->execute();
$result=$sql->get_result();
if ($result===FALSE)
{
  exit ("query fallita");
}

$query1="SELECT * FROM`utenti`WHERE `id_utente`=?";
$sql1 = $conn->stmt_init();
$sql1->prepare ($query1);
$sql1->bind_param("i", $_SESSION['id_user']);
$sql1->execute();
$result1=$sql1->get_result();
if ($result1===FALSE)
{
  exit ("query fallita");
}
$row = mysqli_fetch_array($result);
$row1 = mysqli_fetch_array($result1);
$numeroRighe=mysqli_num_rows($result);
$pdf = new FPDF();
$pdf->SetFont('Arial', 'B', 16);
$pdf->AddPage('L');
$pdf->Cell(40, 10, 'Report biciclette di '.$row1["nome"]);
$pdf->Ln();
$pdf->Ln(9);
$pdf->SetFont('Helvetica', '', 7);
$pdf->Cell(30, 7, 'NUMERO NOLEGGIO', 1);
$pdf->Cell(30, 7, 'NUMERO BICI', 1);
$pdf->Cell(30, 7, 'DATA INIZIO NOL.', 1);
$pdf->Cell(30, 7, 'NUMERO COLONNINA.', 1);
$pdf->Cell(30, 7, 'DATA RESTITUZIONE', 1);
$pdf->Cell(30, 7, 'NUMERO COLONNINA', 1);
$pdf->Ln();
$pdf->SetFont('Helvetica', '', 5);
while($row = mysqli_fetch_array($result))
{
$pdf->Cell(30, 7, $row["id_noleggio"], 1);
$pdf->Cell(30, 7, $row["id_bicicletta"], 1);
$pdf->Cell(30, 7, $row["data_noleggio"], 1);
$pdf->Cell(30, 7, $row["id_stazione_noleggio"], 1);
$pdf->Cell(30, 7, $row["data_restituzione"], 1);
$pdf->Cell(30, 7, $row["id_stazione_restituzione"], 1);
$pdf->Ln();
}
$pdf->Output("storico.pdf",'D'); 
?>
