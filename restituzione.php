<!DOCTYPE html>
<html lang="en">
  <?php
    session_start();
    if (!isset($_SESSION["id_user"]))
        header("location: /bikesharing/login.php");
      include_once "functions.php";
  ?>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Restituisci</title>
    <?php include_once "libraries.php";?>
  </head>
<body>
   <form method="POST" action="do_restituzione.php">
   <div class="container container-fluid" style="margin-top:150px;">
      <div class="row">
        <div class="col-md-6 offset-md-3">
        <p class="text-center">Seleziona la bici che hai noleggiato</p>
            <select id="stationSelector" class="form-control" name="bici">
                        <option selected disabled>SELEZIONA LA BICI:</option>
                            <?php

                                $connection = getDBConnection();
                                if (mysqli_connect_errno())
                                echo "Failed to connect to MySQL: " . mysqli_connect_error();
                                $sql = 'SELECT `id_bicicleta` AS bici,FROM `noleggi` WHERE (`id_utente`=? and (data_restituzione IS NULL OR data_restituzione=="0000-00-00"))';
                                $sql=$connection->prepare($sql);
                                if ($sql === false) echo "Select fallita";
                                $sql->bind_param("i",$_SESSION["id_user"]);
                                $sql->execute();
                                $result=$sql->get_result();
                                while ($row =  mysqli_fetch_array($result))
                                {
                                    echo "<option value=".$row['id_bici'].">".$row['id_bici']."</option>";
                                }
                             ?>
                    </select>
<!---------------------------------------------------------------------------------------------->
            <p class="text-center">Numero Colonnina</p>
            <select id="stationSelector" class="form-control" name="Colonnina">
                        <option selected disabled>SELEZIONA LA COLONNINA:</option>
                            <?php

                                $connection = getDBConnection();
                                if (mysqli_connect_errno())
                                echo "Failed to connect to MySQL: " . mysqli_connect_error();
                                $query = "SELECT id_stazione AS stazione, COUNT(id_bicicletta) AS bici FROM `biciclette` GROUP BY id_stazione HAVING (bici<50)";
                                $query=$connection->prepare($query);
                                if ($query === false) echo "Select fallita";
                                $query->execute();
                                $result=$query->get_result();
                                while ($row =  mysqli_fetch_array($result))
                                {
                                    echo "<option value=".$row['stazione'].">".$row['stazione']."</option>";
                                }
                             ?>
                    </select>
            <br><br>
            <div class="centrato">
                <input type="submit"  class="btn btn-primary" value="RESTITUISCI">
            </div>
        </div>
    </form>
    </div>
      </div>
    </div>
</body>
</html>
