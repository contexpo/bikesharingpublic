<?php
session_start();

if (!isset($_SESSION["id_user"]))
{
	header("location: /bikesharing/login.php");
}

if (!isset($_GET["station_id"]))
{
	die(header("HTTP/1.0 400 Bad Request"));
}

include_once "functions.php";


$connection = getDBConnection();

if (mysqli_connect_errno()) echo "Failed to connect to MySQL: " . mysqli_connect_error();
$sql = "SELECT id_bicicletta FROM `biciclette` WHERE id_stazione=? ORDER BY id_bicicletta";
$sql = $connection->prepare($sql);
$sql->bind_param("i", $_GET["station_id"]);

if ($sql === false)
{
	echo "Select fallita";
}
$sql->execute();
$result = $sql->get_result();

if ($result->num_rows == 0)
{
	echo ("null");
	die();
}
?>
<label for="bikeSelector">Numero della bici</label>
<select id="bikeSelector" class="form-control" name="bike_id">
<option value="" selected disabled hidden>Seleziona bici</option>
<?php
while ($row = mysqli_fetch_array($result))
{
	echo '<option value="' . $row["id_bicicletta"] . '">' . $row["id_bicicletta"] . '</option>';
}

?>
</select>