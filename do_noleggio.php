<?php
session_start();

if (!isset($_SESSION["id_user"]))
{
    header("location: /bikesharing/login.php");
}

if (!isset($_GET["station_id"], $_GET["bike_id"]))
{
    header('Location:/bikesharing/noleggi.php');
}

include_once "functions.php";

$conn = getDBConnection();

// verifica su eventuali errori di connessione

if ($conn->connect_errno)
{
	echo "connessione fallita: " . $conn->connect_error . ".";
	exit();
}

$conn->autocommit(FALSE);
$conn->begin_transaction(MYSQLI_TRANS_START_READ_WRITE);

$query = "SELECT id_stazione, stazioni.nbici FROM biciclette WHERE (id_bicicletta=? AND  biciclette.id_stazione=stazioni.id_stazione)";
$sql=$conn->prepare($query);
$sql->bind_param("i", $_GET['bike_id']);
$sql->execute();
$result=$sql->get_result();
$row=$result->fetch_assoc();
if ($row["id_stazione"]=="NULL" || $conn->errno || (int)$row["id_stazione"]>49)
{
	$conn->rollback();
	header("Location: /bikesharing/noleggi.php?error=1");
}

$query = "UPDATE `biciclette` SET id_stazione=null WHERE (id_bicicletta=?);
ALTER TABLE stazioni SET nbici=nbici-1 WHERE (id_stazione=?);";
$sql=$conn->prepare($query);
$sql->bind_param("ii", $_GET["bike_id"], $_GET["station_id"]);
$sql->execute();

if ($conn->errno)
{
	$conn->rollback();
	header("Location: /bikesharing/noleggi.php?error=1");
}

$query = "INSERT INTO `noleggi` (`id_utente`, `id_bicicletta`, `data_noleggio`, `id_stazione_noleggio`) VALUES (?, ? , NOW(), ?);";
$sql=$conn->prepare($query);
$sql->bind_param("iii", $_SESSION["id_user"], $_GET["bike_id"], $_GET["station_id"]);
$sql->execute();

if ($conn->errno)
{
	$conn->rollback();
	header("Location: /bikesharing/noleggi.php?error=1");
}

$conn->commit();
$conn->close();
header ("Location:/bikesharing/restituzione.php");
?>
