<?php 
session_start();
if (isset($_SESSION["id_user"]))
    header("Location: /bikesharing/noleggi.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include_once "libraries.php"; ?>
</head>

<body class="text-center">
    <div class="row" style="margin-top:150px;">
        <div class="col-md-4 offset-md-4">
            <form method="POST" class="form-signin" action="/bikesharing/do_login.php">
                <!--<img class="mb-4" src="login2_files/bootstrap-solid.svg" alt="" width="72" height="72">-->
                <h1 class="h3 mb-3 font-weight-normal">Login</h1>
                <label for="username" class="sr-only">Username</label>
                <input type="text" id="username" name="username" class="form-control" placeholder="Username" required="" autofocus="">
                <label for="Password" class="sr-only">Password</label>
                <input type="password" id="Password" name="password" class="form-control" placeholder="Password" required="">
                <input type="hidden" name="returnUrl" value=<?php if (isset ($_GET["returnUrl"])) echo $_GET["returnUrl"];?>> 
                <button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
                <p class="text-muted">Se non hai un account, <a href="/bikesharing/register.php">registrati ora</a></p>
            </form>
        </div>
    </div>

</body>

</html>