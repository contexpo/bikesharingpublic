<html>
    <head><title>STORICO</title>
    <?php include_once "libraries.php";  ?>
    <body>
        <form method="POST" action="invia.php">
        <?php
        session_start();
          if (!isset($_SESSION["id_user"]))
        {
            header("location: /bikesharing/login.php");
        }
        include_once "functions.php";
         $conn = getDBConnection();
        if ($conn->connect_errno)
        {
            echo "connessione fallita: " . $conn->connect_error . ".";
            exit();
        }
        $query="SELECT * FROM`noleggi`WHERE `id_utente`=?";
        $sql = $conn->stmt_init();
        $sql->prepare ($query);
        $sql->bind_param("i", $_SESSION['id_user']);
        $sql->execute();
        $result=$sql->get_result();
        if ($result===FALSE)
        {
            exit ("query fallita");
        }
        echo "<table class='table'>";
                echo "<tr scope='row'>";
                echo "<th>ID Nolegigio</th>";
                echo "<th>ID Biciceltta</th>";
                echo "<th>ID Data Noleggio</th>";
                echo "<th>ID stazione noleggio</th>";
                echo "<th>ID Data restituzione</th>";
                echo "<th>Stazione restituzione</th>";
                echo"</tr>";
       while($row=mysqli_fetch_array($result))
            {
                echo "<tr scope='row'>";
                echo"<td>".$row["id_noleggio"]."</td>";
                echo "<td>".$row["id_bicicletta"]."</td>";
                echo "<td>".$row["data_noleggio"]."</td>";
                echo "<td>".$row["id_stazione_noleggio"]."</td>";
                echo "<td>".$row["data_restituzione"]."</td>";
                echo "<td>".$row["id_stazione_restituzione"]."</td>";
                echo"</tr>";
            }
        echo"</table>";
    ?>
     <input class="btn btn-primary" type="submit" value="SCARICA STORICO" >
    </form>
    </body>
</html>
