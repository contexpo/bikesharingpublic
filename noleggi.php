<!DOCTYPE html>
<html lang="en">
  <?php
    session_start();
    if (!isset($_SESSION["id_user"])) 
    {
        header("location: /bikesharing/login.php?returnUrl=".urlencode($_SERVER['REQUEST_URI']));
    }
      include_once "functions.php";
  ?>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php include_once "libraries.php";?>
    <title>Noleggi</title>
  </head>
  <body>
  <?php include_once "navbar.php"?>
    <div class="container container-fluid" style="margin-top:150px;">
      <div class="row">
        <div class="col-md-8 offset-md-2">
          <h4 class="text-center">Noleggio bici</h4>
        <form action="do_noleggio.php" method="get">
        <label for="stationSelector">Colonnina</label>
        <select id="stationSelector" class="form-control" name="station_id">
        <option value="" selected disabled hidden>Seleziona la colonnina</option>
        <?php
        $connection = getDBConnection();
        if (mysqli_connect_errno()) echo "Failed to connect to MySQL: " . mysqli_connect_error();
        $sql = "SELECT * FROM stazioni ORDER BY stazioni.id_stazione";
        $sql = $connection->prepare($sql);
        if ($sql === false) echo "Select fallita";
        $sql->execute();
        $result = $sql->get_result();
        while ($row = mysqli_fetch_array($result))
        {
          printf('<option value="%d">%s - %d bici libere</option>', $row["id_stazione"], $row["nome"], $row["nBici"]);
        }
        ?>
        </select>
        <div name="bikeSelectorDiv" id="bikeSelectorDiv"></div>
        <input id="btnForm" class="btn btn-primary" type="submit" value="Noleggia" style="display: none;">
        </form>
      </div>
      </div>
    </div>
      <script>
    $("#stationSelector").change(function () {
        $("#bikeSelectorDiv").empty();
        $("#btnForm").hide();
        $.ajax({
          type: "get",
          url: "/bikesharing/getBicicletteLibere.php",
          data: {station_id: $(this).val()},
          dataType: "html",
          success: function (response) {
            if (response=="null")
            {
              $("#bikeSelectorDiv").text("Nessuna bicicletta disponibile in quella colonnina");
            }
            else
            {
              $("#btnForm").show();
              $("#bikeSelectorDiv").html(response);
            }
          }
        });
        $("#bikeSelectorDiv").html('<div class="d-flex justify-content-center"><div class="spinner-border" role="status"><span class="sr-only">Loading...</span></div></div>');
    });
    </script>
  </body>
</html>
