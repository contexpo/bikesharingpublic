<!DOCTYPE html>
<html lang="en">
<meta charset="UTF-8">
<head>
    <?php include_once "libraries.php"; ?>
</head>

<body class="text-center">
    <div class="row" style="margin-top:4rem;">
        <div class="col-md-4 offset-md-4">
        <form method="POST" action="do_register.php">
            <label for="nome" class="form-inline">Nome</label>
            <input type="text" name="nome" class="form-control" placeholder="Inserisci il tuo nome">
            <label for="cognome" class="form-inline">Cognome</label>
            <input type="text" name="cognome" class="form-control" placeholder="Inserisci il tuo cognome">
            <label for="" class="form-inline">Indirizzo</label>
            <input type="text" name="address" class="form-control" placeholder="Inserisci il tuo indirizzo">
            <label for="" class="form-inline">Citta'</label>
            <input type="text" name="city" class="form-control" placeholder="Inserisci la tua città">
            <label for="" class="form-inline">Email</label>
            <input type="text" name="email" class="form-control" placeholder="Inserisci la tua email">
            <label for="" class="form-inline">Username</label>
            <input type="text" name="username" class="form-control" placeholder="Inserisci il tuo username">
            <label for="" class="form-inline">Password</label>
            <input type="password" name="password" class="form-control" placeholder="Inserisci la tua password">
            <input type="submit" class="btn btn-primary" value="Registrati"> 
            <p> Oppure <a href="/bikesharing/login.php"> effettua il login </a></p>
    </form>
        </div>
    </div>

</body>

</html>