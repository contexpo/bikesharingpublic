<?php
include_once "functions.php";
$waypoints="[";
$connection = getDBConnection();
if (mysqli_connect_errno()) echo "Failed to connect to MySQL: " . mysqli_connect_error();
$sql = "SELECT `nome`, `nbici`, `lon`, `lat` FROM `stazioni`";
$sql = $connection->prepare($sql);
if ($sql === false) echo "Select fallita";
$sql->execute();
$result = $sql->get_result();
$defaultString='{"name":"%s", "lon":"%f", "lat":"%f", "details":"%s"},';
while ($row = mysqli_fetch_array($result))
{
    $waypoints= $waypoints.sprintf($defaultString, $row["nome"], $row["lon"], $row["lat"], $row["nome"]." - ".$row['nbici']." posti occupati");
}
$waypoints=$waypoints."]";
echo $waypoints;
 /*'[{"name":"Tunbridge Wells, Langton Road, Burnt Cottage",
  "lon":"0.213102",
  "lat":"51.1429",
  "details":"A Grade II listed five bedroom wing in need of renovation."}]'*/
  ?>