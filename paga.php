<!DOCTYPE html>
<html lang="en">
<?php
    session_start();
    if (!isset($_SESSION["id_user"]))
    {
        header("location: /bikesharing/login.php?returnUrl=".urlencode($_SERVER['REQUEST_URI']));
    }
      include_once "functions.php";
  ?>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<?php include_once "libraries.php";?>
    <title>Pagamenti</title>
</head>

<body>
    <?php include_once "navbar.php"?>
  <form method="POST" action="pagato.php">
	<h4 class="text-center" style="margin-top:60px;">Noleggi Non Ancora Pagati</h4>
    <div class="container container-fluid" style="margin-top:80px;">
        <div class="row">
            <div class="col-md-8 offset-md-2">
					<?php
						$conn = getDBConnection();

						// verifica su eventuali errori di connessione

						if ($conn->connect_errno)
						{
							echo "conn fallita: " . $conn->connect_error . ".";
							exit();
						}

                        $query="SELECT * FROM noleggi WHERE(id_utente=? AND pagato=0)";
                        $sql=$conn->prepare ($query);
                        $sql->bind_param("i", $_SESSION['id_user']);
                        $sql->execute();
                        $result=$sql->get_result();;
						?>
        <table class="table table-striped" style="margin-top:10vh">
  				<thead><td>ID Noleggio</td><td>ID Utente</td><td>ID Bicicletta</td><td>Data Noleggio</td><td>ID Stazione noleggio</td></thead>
    				<tbody>
    				<?php
      				while ($row = mysqli_fetch_array($result))
      				{
      					printf('<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>', $row["id_noleggio"], $row["id_utente"],$row["id_bicicletta"], $row["data_noleggio"], $row["id_stazione_noleggio"]);
      				}
    				?>
    				  </tbody>
				     </table>
				  </tbody>
				</table>
		      <input class="btn btn-primary" type="submit" value="Paga">
            </div>
        </div>
    </div>
  </form>
</body>

</html>
