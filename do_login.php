<?php
session_start();

if (!isset($_POST["username"], $_POST["password"])) 
{
    header('Location:/bikesharing/login.php');
}

include_once "functions.php";

$user = $_POST['username'];
$password = crypt($_POST["password"], "stringadisalt");

// /////////////////////////////////////

$conn = getDBConnection();

// verifica su eventuali errori di connessione

if ($conn->connect_errno)
{
	echo "conn fallita: " . $conn->connect_error . ".";
	exit();
}

$sql = "SELECT * FROM utenti WHERE username=? AND `password`=?";
$sql = $conn->prepare($sql);

if ($sql === false) exit();
$sql->bind_param("ss", $user, $password);
$sql->execute();
$result = $sql->get_result();

if ($result === FALSE)
{
	exit("query fallita");
}
else
if ($result->num_rows == 1)
{
	$row = $result->fetch_assoc();
	$_SESSION["id_user"] = $row["id_utente"];
	$_SESSION["username"] = $_POST['username'];
	if (isset($_POST["returnUrl"]))
	{
		header('Location:/'.urldecode($_POST["returnUrl"]));
	}
	else
	{
		header('Location:/bikesharing/noleggi.php');
	}
	
}

$numero = $result->num_rows;
echo $numero;
$conn->close();
?>
