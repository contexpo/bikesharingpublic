<?php
session_start();

if (!isset($_SESSION["id_user"])) 
{
    header("location: /bikesharing/login.php"); 
}

if (!isset($_POST["n_carta"], $_POST["cvv"], $_POST["expiry"])) 
{
    header('Location:/bikesharing/payments.php');
}
include_once "functions.php";
$conn = getDBConnection();
// verifica su eventuali errori di connessione
if ($conn->connect_errno)
    exit("errore di connessione");
$query="INSERT INTO `metodipagamento` (`id_utente`, `n_carta`, `cvv`, `expiry`) VALUES (?, ?, ?, ?);";
$sql=$conn->prepare($query);
$sql->bind_param("isss", $_SESSION["id_user"], $_POST["n_carta"], $_POST["cvv"], $_POST["expiry"]);
$sql->execute();
$result = $sql->get_result();
?> 