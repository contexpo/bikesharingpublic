<!DOCTYPE html>
<html lang="en">
<?php
session_start();

if (!isset($_SESSION["id_user"])) 
{
    header("location: /bikesharing/login.php?returnUrl=".urlencode($_SERVER['REQUEST_URI']));
}
include_once "functions.php";

?>

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Mappa</title>
        <?php
            include_once "libraries.php";
        ?>
    </head>

    <body>
    <?php include_once "navbar.php"?>
        <div class="container-fluid text-center" style="margin-top: 20px;">
            <div class="row">
                <div class="col-md-6 offset-md-3">
                    <div id="map" style="height: 750px;">
                    </div>
                </div>
            </div>
        </div>
    </body>
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.4.0/dist/leaflet.css" integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA==" crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.4.0/dist/leaflet.js" integrity="sha512-QVftwZFqvtRNi0ZyCtsznlKSWOStnDORoefr1enyq5mVL4tmKB3S/EnC3rRJcxCPavG10IcrVGSmPh6Qw5lwrg==" crossorigin=""></script>
    <script>

function initmap() 
{
    // set up the map
    map = new L.Map('map');

    // create the tile layer with correct attribution
    var osmUrl='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
    var osmAttrib='Map data © <a href="https://openstreetmap.org">OpenStreetMap</a> contributors';
    var osm = new L.TileLayer(osmUrl, {attribution: osmAttrib});		

    // start the map in South-East England
    map.setView(new L.LatLng(45.5154, 8.0688),12);
    map.addLayer(osm);
}

function removeMarkers() 
{
    for (i=0;i<plotlayers.length;i++)
    {
        map.removeLayer(plotlayers[i]);
    }
    plotlayers=[];
}

function askForPlots() 
{
	/* request the marker info with AJAX for the current bounds
	var bounds=map.getBounds();
	var minll=bounds.getSouthWest();
	var maxll=bounds.getNorthEast();
	var msg='leaflet/findbybbox.cgi?format=leaflet&bbox='+minll.lng+','+minll.lat+','+maxll.lng+','+maxll.lat;
	ajaxRequest.onreadystatechange = stateChanged;
	ajaxRequest.open('GET', msg, true);
    ajaxRequest.send(null);*/
    return '<?php include "getMapTooltips.php" ?>';
}

// then add this as a new function...
function onMapMove(e) 
{ 
    askForPlots(); 
}

    var map;
    var ajaxRequest;
    var plotlist;
    var plotlayers=[];
    initmap();
    plotlist=eval("(" + '<?php include "getMapTooltips.php" ?>' + ")");
    removeMarkers();
    for (i=0;i<plotlist.length;i++) 
    {
        var plotll = new L.LatLng(plotlist[i].lat,plotlist[i].lon, true);
        var plotmark = new L.Marker(plotll);
        plotmark.data=plotlist[i];
        map.addLayer(plotmark);
        plotmark.bindPopup("<h3>"+plotlist[i].name+"</h3>"+plotlist[i].details);
        plotlayers.push(plotmark);
    }
    askForPlots();
	map.on('moveend', onMapMove);

</script>
</html>