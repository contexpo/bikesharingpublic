<!DOCTYPE html>
<html lang="en">
<?php 
    session_start();
    if (!isset($_SESSION["id_user"])) 
    {
        header("location: /bikesharing/login.php?returnUrl=".urlencode($_SERVER['REQUEST_URI']));
    }
      include_once "functions.php";
  ?>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<?php include_once "libraries.php";?>
    <title>Pagamenti</title>
</head>

<body>
    <?php include_once "navbar.php"?>
	<h4 class="text-center" style="margin-top:60px;">Pagamenti</h4>
    <div class="container container-fluid" style="margin-top:80px;">
        <div class="row">
            <div class="col-md-8 offset-md-2">
				<form method="POST" action="./add_payment_method.php">
					<h5>Nuovo metodo di pagamento</h5>
					<label for="intestatario" class="form-inline">Intestatario</label>
					<input type="text" name="intestatario" class="form-control" placeholder="Intestatario" disabled value=
					<?php 
						$conn = getDBConnection();

						// verifica su eventuali errori di connessione

						if ($conn->connect_errno)
						{
							echo "conn fallita: " . $conn->connect_error . ".";
							exit();
						}

						$sql = "SELECT * FROM utenti WHERE username=?";
						$sql = $conn->prepare($sql);

						if ($sql === false) exit();
						$sql->bind_param("s", $_SESSION["username"]);
						$sql->execute();
						$result = $sql->get_result();
						$row=$result->fetch_assoc();
						if ($result === FALSE)
						{
							exit("query fallita");
						}
						else
						{
							$stringa='\'' .$row["cognome"]." ".$row["nome"].'\'';
							echo trim($stringa);
						}
						$conn->close();
						?>
						>
					<label for="expiry" class="form-inline">Numero di carta</label>
					<input type="text" name="n_carta" class="form-control" placeholder="Numero di carta" required maxlength="20">
					<label for="expiry" class="form-inline">Data di scadenza</label>
					<input type="text" name="expiry" class="form-control" placeholder="MM/YY" required maxlength="5">
					<label for="cvv" class="form-inline" inputmode="numeric">CVV</label>
					<input type="text" name="cvv" class="form-control" placeholder="Inserisci il CVV" required inputmode="numeric" maxlength="3">
					<input type="submit" value="Inserisci" class="btn btn-primary">
				</form>
				<?php 
					$connection = getDBConnection();
					if (mysqli_connect_errno()) echo "Failed to connect to MySQL: " . mysqli_connect_error();
					$sql = "SELECT * FROM metodipagamento, utenti WHERE (metodipagamento.id_utente=? AND metodipagamento.id_utente=utenti.id_utente)";
					$sql = $connection->prepare($sql);
					if ($sql === false) echo "Select fallita";
					$sql->bind_param("i", $_SESSION["id_user"]);
					$sql->execute();
					$result = $sql->get_result();
					if ($result->num_rows>0):?>
				<table class="table table-striped" style="margin-top:10vh">
				<thead><td>Numero di carta</td><td>Intestatario</td><td>Data di scadenza</td><td>CVV</td></thead>
				<tbody>
				<?php
				while ($row = mysqli_fetch_array($result))
				{
					printf('<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>', $row["n_carta"], $row["nome"]." ".$row["cognome"], $row["expiry"], $row["cvv"]);
				}
				?>
				</tbody>
				</table>
				<?php endif?>
            </div>
        </div>
    </div>
</body>

</html>